local CurrentVersion = "v3.19";
local UpdateLogs = {
    ["v3.19"] = {
        "Spinning Confirmation";
        "2x Experience Weekends";
        "Update Log UI";
        "Reduced Spawn Time After Death";
        "Raid Village Notification for Console";
        "In-Game Titles";
        "Dungeon and Mission Rewards";
    };
};

UpdateLogs["GameVersion"] = CurrentVersion;
return UpdateLogs;